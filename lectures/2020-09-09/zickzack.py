from turtle import*
reset()
speed(1)

left(45)
pensize(5)
pencolor("blue")

forward(100)
right(90)
pencolor("red")
forward(100)
left(90)
pencolor("cyan")
forward(100)
right(90)
pencolor("black")
forward(100)

