# Simon Mettler

import turtle as t

t.reset()
t.speed(50)
t.pensize(3)
t.pencolor('red')

def square(angle, fill):
    t.lt(angle)
    t.fillcolor(fill)
    t.begin_fill()
    t.forward(100)
    t.rt(90)
    t.forward(100)
    t.rt(90)
    t.forward(100)
    t.rt(90)
    t.forward(100)
    t.rt(90)
    t.end_fill()
    t.home()
    
square(36, 'cyan')
square(324, 'yellow')
square(252, 'magenta')
square(180, 'blue')
square(108, 'green')

