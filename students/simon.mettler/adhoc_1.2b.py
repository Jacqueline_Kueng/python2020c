# Simon Mettler

import turtle as t

t.reset()

# draw rectangle
t.forward(200)
t.lt(90)
t.forward(100)
t.lt(90)
t.forward(200)
t.lt(90)
t.forward(100)