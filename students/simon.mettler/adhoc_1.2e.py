# Simon Mettler

import turtle as t

t.reset()
t.pensize(3)

t.pencolor('red')
t.forward(100)
t.lt(120)
t.forward(100)
t.lt(120)
t.forward(100)

t.pencolor('blue')
t.rt(120)
t.forward(100)
t.lt(120)
t.forward(100)
t.lt(120)
t.forward(100)

t.pencolor('green')
t.rt(120)
t.forward(100)
t.lt(120)
t.forward(100)
t.lt(120)
t.forward(100)
