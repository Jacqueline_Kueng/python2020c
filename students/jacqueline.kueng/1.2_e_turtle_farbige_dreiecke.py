# ad hoc übung 1.2 e) drei Dreiecke 


from turtle import*
name= "turtle"
speed= 100

pencolor("blue")
pensize(5)

# 35 Einheiten vorwärts gehen
forward(35)
# Um 120° Grad nach links drehen
left (120)
# 135 Einheiten vorwärts gehen
forward (135)
# Um 120° Grad nach links drehen
left(120)
# 135 Einheiten vorwärts gehen
forward(135)
# Um 120° Grad nach links drehen
left(120)
# wieder um 100 vorwärts gehen um Dreieck zu schliessen
forward(100)


penup()
forward(130)
pendown()

pencolor("red")
pensize(5)

# 35 Einheiten vorwärts gehen
forward(35)
# Um 120° Grad nach links drehen
left (120)
# 135 Einheiten vorwärts gehen
forward (135)
# Um 120° Grad nach links drehen
left(120)
# 135 Einheiten vorwärts gehen
forward(135)
# Um 120° Grad nach links drehen
left(120)
# wieder um 100 vorwärts gehen um Dreieck zu schliessen
forward(100)


penup()
right(90)
forward(120)
left (90)
backward(120)
forward(50)
pendown()


pencolor("lightgreen")
pensize(5)
# 35 Einheiten vorwärts gehen
forward(35)
# Um 120° Grad nach links drehen
left (120)
# 135 Einheiten vorwärts gehen
forward (135)
# Um 120° Grad nach links drehen
left(120)
# 135 Einheiten vorwärts gehen
forward(135)
# Um 120° Grad nach links drehen
left(120)
# wieder um 100 vorwärts gehen um Dreieck zu schliessen
forward(100)


# Turtle in Mitte bringen:
penup()
left(90)
forward(120)
right (90)
backward(120)
forward(90)
right(240)
pendown()



print(turtle)