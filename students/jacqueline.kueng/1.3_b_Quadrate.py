# ad hoc übung 1.3 Quadrate


from turtle import*
name= "turtle"
speed=(100)

left(90)

begin_fill()
pencolor("red")
fillcolor("blue")
pensize(5)
forward(90)
left(90)
forward(90)
left(90)
forward(90)
left(90)
forward(90)
end_fill()


# Achendrehung:
left(25)


begin_fill()
pencolor("red")
pensize(5)
fillcolor("lightgreen")
forward (90)
left(90)
forward(90)
left(90)
forward(90)
left(90)
forward(90)
end_fill()

# Achendrehung:
left(230)


begin_fill()
pencolor("red")
pensize(5)
fillcolor("magenta")
forward (90)
left(90)
forward(90)
left(90)
forward(90)
left(90)
forward(90)
end_fill()

# Achendrehung:
right(120)


begin_fill()
pencolor("red")
pensize(5)
fillcolor("cyan")
forward (90)
left(90)
forward(90)
left(90)
forward(90)
left(90)
forward(90)
end_fill()


# Achendrehung:
left(35)

begin_fill()
pencolor("red")
pensize(5)
fillcolor("yellow")
forward (90)
left(90)
forward(90)
left(90)
forward(90)
left(90)
forward(90)
end_fill()

print(turtle)